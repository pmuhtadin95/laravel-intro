<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function submit(Request $request){
        //dd($request->all());
        $nama = $request["nama_depan"];
        //return "$nama";
        return view('welcome')->with(['nama' => $nama]);
    }
}
