<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="">First Name:</label><br>
        <input type="text" name="nama_depan"><br>

        <label for="">Last Name:</label><br>
        <input type="text" name="nama_belakanga"><br><br>

        <label for="">Gender</label><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Male">Female<br>
        <input type="radio" name="gender" value="Male">Others<br><br>

        <label>Nationality:</label><br>
        <select name="" id="">
            <option value="1">Indonesia</option>
            <option value="2">Thailand</option>
            <option value="2">Wakanda</option>
        </select><br><br>

        <label for="">Language Spoken:</label><br>
        <input type="checkbox" name="language" value="indo">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="english">English<br>
        <input type="checkbox" name="language" value="other">Other<br><br>

        <label for="">Bio:</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit">
        
    </form>

</body>
</html>